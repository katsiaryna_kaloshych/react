import React from 'react';
import './App.css';
import Header from './components/Header';
import Navbar from './components/Navbar';
import Profile from './components/Profile';
import DialogContainer from './components/DialogContainer';
import Music from './components/Music';
import {Route} from 'react-router-dom';

const App = (props) => {
  return (
   
      <div className='app-wrapper'>
        <Header />
        <Navbar />
        <div className='app-wrapper-content'>
          <Route path='/dialog' render={ ()=><DialogContainer /> }/>
          <Route path='/profile' render={()=><Profile />} />
          <Route path='/music' component={Music} />
        </div>
      </div>

  );
}

export default App;