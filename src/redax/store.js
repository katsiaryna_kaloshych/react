import ProfileReducer from './profile-reducer';
import DialogsReducer from './dialogs-reducer';

let store = {
    _state: {
        profilesPage:
        {
            posts: [
                { id: 1, message: 'HI', likeCount: 12 },
                { id: 2, message: 'HELLO', likeCount: 3 }
            ],
            newPostText: 'it'
        },
        dialogsPage:
        {
            dialogs: [
                { id: 1, name: 'Kate' },
                { id: 2, name: 'Vitaly' },
                { id: 3, name: 'Anna' },
                { id: 4, name: 'Artem' },
                { id: 5, name: 'Richy' }
            ],
            messages: [
                { id: 1, message: 'HI' },
                { id: 2, message: 'HELLO' },
                { id: 3, message: 'OLA' },
                { id: 4, message: 'How are you?' }
            ],
            newMessageBody: ''
        }
    },
    getState() {
        return this._state;
    },
    _callSubscriber() {
        console.log("state is changed");
    },
    dispatch(action) {
        this._state.profilesPage=ProfileReducer(this._state.profilesPage, action);
        this._state.dialogsPage=DialogsReducer(this._state.dialogsPage, action);
            this._callSubscriber(this._state);
    },


    subscribe(observer) {
        this._callSubscriber = observer;
    }

}

export default store;
window.store = store;