import {createStore, combineReducers} from 'redux';
import ProfileReducer from './profile-reducer';
import  DialogsReducer from './dialogs-reducer'

let reducers=combineReducers({
    profilesPage: ProfileReducer,
    dialogsPage: DialogsReducer
})

let store=createStore(reducers);

export default store;