const UPDATE_NEW_MESSAGE_BODY = 'UPDATE-NEW-MESSAGE-BODY';
const SEND_MASSAGE = 'SEND-MASSAGE';

let initialState={
    dialogs: [
        { id: 1, name: 'Kate' },
        { id: 2, name: 'Vitaly' },
        { id: 3, name: 'Anna' },
        { id: 4, name: 'Artem' },
        { id: 5, name: 'Richy' }
    ],
    messages: [
        { id: 1, message: 'HI' },
        { id: 2, message: 'HELLO' },
        { id: 3, message: 'OLA' },
        { id: 4, message: 'How are you?' }
    ],
    newMessageBody: ''
}

const DialogsReducer=(state=initialState, action)=>{
   switch(action.type){
       case UPDATE_NEW_MESSAGE_BODY:
            state.newMessageBody = action.body;
            return state;
        case SEND_MASSAGE:
            let body = state.newMessageBody;
           state.newMessageBody = '';
           state.messages.push({ id: 5, message: body })
           return state;
           default: return state;
        }
    
}

export const sendMessageCreator = (message) => ({ type: SEND_MASSAGE, id: 5, message: message })

export const updateNewMessageBodyCreator = (body) => ({ type: UPDATE_NEW_MESSAGE_BODY, body: body})

export default DialogsReducer;