import React from 'react';
import * as serviceWorker from './serviceWorker';
import store from './redax/redux-store';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import StoreContext from './StoreContext'

let renderEntireTree=()=>{
    ReactDOM.render(
        <BrowserRouter>
        <StoreContext value={store}>
        <App/>
        </StoreContext>
        </BrowserRouter>, document.getElementById('root') );
}


renderEntireTree();

store.subscribe(()=>{
  renderEntireTree()
});

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
