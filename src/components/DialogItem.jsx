import React from 'react';
import s from './../css/Dialog.module.css';
import {NavLink} from 'react-router-dom';


const DialogItem=(props)=>{
   
    return(
    <div className={s.dialog}>
    <NavLink to={"dialogs/"+ props.id}> {props.name}</NavLink>  
   </div>
    );
}

export default DialogItem;