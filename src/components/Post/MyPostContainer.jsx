import React from 'react';
import MyPost from './MyPost';
import { addPostActionCreator, updateNewPostTextActionCreator } from './../../redax/profile-reducer'
import StoreContext from '../../StoreContext';
import store from '../../redax/redux-store';


const MyPostContainer = () => {
    return(
         <StoreContext.Consumer>
            {
            (store) => {
                let state =store.getState();

                let addPost = () => {
                   store.dispatch(addPostActionCreator())
                };

                let onPostChange = (text) => {
                    store.dispatch(updateNewPostTextActionCreator(text));
                }
                return  <MyPost updateNewPostText={onPostChange} addPost={addPost}
                        posts={state.profilesPage.posts}
                        newPostText={state.profilesPage.newPostText} />
            }
            }
        </StoreContext.Consumer>
    )
}

export default MyPostContainer;