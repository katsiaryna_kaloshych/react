import React from 'react';
import s from './../../css/MyPost.module.css'
import Post from './Post';
import {addPostActionCreator, updateNewPostTextActionCreator} from './../../redax/profile-reducer'


const MyPost=(props)=>{
   
    let posts=props.posts.map(p=><Post message={p.message} likeCount={p.likeCount}/>)
    
    let onAddPost=()=>{
        props.addPost()
        };

    let newPostElement=React.createRef();

    let onPostChange=()=>{
        let text=newPostElement.current.value;
        props.updateNewPostText(text);
    }

    return <div>
        My post
<div>
New post
</div>
<textarea onChange={onPostChange} ref={newPostElement} value={props.newPostText}/>
<button onClick={onAddPost}>Add post</button>

<div className={s.posts}>
{posts}
</div>
    </div>
}

export default MyPost;