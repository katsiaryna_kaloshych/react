import React from 'react';
import Dialog from './Dialog';
import { updateNewMessageBodyCreator, sendMessageCreator } from '../redax/dialogs-reducer';
import { connect } from 'react-redux';

let mapStateToProps=(state)=>{
    return{
 dialogsPage:state.dialogsPage
    }
}

let mapDispatchToProps =(dispatch)=>{
    return{
        onSendMessageClick:()=>{
            dispatch(sendMessageCreator());
        },
        onNewMessageChange:(body)=>{
            dispatch(updateNewMessageBodyCreator(body));
        }
    }
}

const DialogContainer=connect(mapStateToProps, mapDispatchToProps)(Dialog);

export default DialogContainer;