import React from 'react';
import s from './../css/Dialog.module.css';
import DialogItem from './DialogItem';
import Message from './Message'
import { updateNewMessageBodyCreator, sendMessageCreator } from '../redax/dialogs-reducer';

const Dialog=(props)=>{
let state=props.dialogsPage;

    let dialogElement=state.dialogs.map(d=><DialogItem name={d.name} id={d.id}/>);
    let messagesElement=state.messages.map(m=> <Message message={m.message}/>);
    let newMessageBody=state.newMessageBody;
    

let onSendMessageClick=()=>{
    props.onSendMessageClick();
}
let onNewMessageChange=(e)=>{
    let body=e.target.value;
    props.onNewMessageChange(body);
}

    return(
        <div className={s.dialogs}>
            <div className={s.dialogItem}>
         {dialogElement}
           </div>
           <div className={s.messages}>
            <div>{messagesElement}</div>
            <div>
                <div><textarea ploceholder='Enter you message' value={newMessageBody} onChange={onNewMessageChange}></textarea></div>
                <div><button onClick={onSendMessageClick}>send</button>

                </div>
            </div>

           </div>
        </div>
    )
}

export default Dialog;